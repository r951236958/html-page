;(function () {
  var sheetid = '1sQof_GnO64hD9TnLqBjpzmzKZ2g_J8-9zLZwbuG4psY',
    sheetno = 3,
    dataurl = 'https://spreadsheets.google.com/feeds/list/' + sheetid + '/' + sheetno + '/public/values?alt=json-in-script&callback=?'

  $.getJSON(dataurl, function (json) {
    var d = json.feed.entry
    var items = []
    for (var i = 0; i < d.length; i++) {
      var item = {}
      item.number = d[i].gsx$number.$t
      item.name = d[i].gsx$item.$t
      item.npc01 = d[i].gsx$npc01.$t
      item.npc02 = d[i].gsx$npc02.$t
      item.npc03 = d[i].gsx$npc03.$t
      item.npc04 = d[i].gsx$npc04.$t
      item.npc05 = d[i].gsx$npc05.$t
      item.npc06 = d[i].gsx$npc06.$t
      item.npc07 = d[i].gsx$npc07.$t
      item.npc08 = d[i].gsx$npc08.$t
      item.npc09 = d[i].gsx$npc09.$t
      item.npc10 = d[i].gsx$npc10.$t
      items.push(item)
    }
    for (var i = 0; i < d.length; i++) {
      var tableData =
        '<tr>' +
        '<th class="number d-none">' +
        items[i].number +
        '</th>' +
        '<td class="item">' +
        items[i].name +
        '</td>' +
        '<td class="npc01">' +
        '<img src="' +
        items[i].npc01 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc02">' +
        '<img src="' +
        items[i].npc02 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc03">' +
        '<img src="' +
        items[i].npc03 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc04">' +
        '<img src="' +
        items[i].npc04 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc05">' +
        '<img src="' +
        items[i].npc05 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc06">' +
        '<img src="' +
        items[i].npc06 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc07">' +
        '<img src="' +
        items[i].npc07 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc08">' +
        '<img src="' +
        items[i].npc08 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc09">' +
        '<img src="' +
        items[i].npc09 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td>' +
        '<td class="npc10">' +
        '<img src="' +
        items[i].npc10 +
        '"class="gift-item-img" title="' +
        items[i].name +
        '" alt=""></td></tr>'

      $('.gift-list-item').append(tableData)
    }
  })
})()

function myFunction() {
  var input, filter, table, tr, td, i, txtValue
  input = document.getElementById('myInput')
  filter = input.value.toUpperCase()
  table = document.getElementById('acnh')
  tr = table.getElementsByTagName('tr')
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName('td')[0]
    if (td) {
      txtValue = td.textContent || td.innerText
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = ''
      } else {
        tr[i].style.display = 'none'
      }
    }
  }
}
