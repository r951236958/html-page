(function() {
  var sheetid = '1sQof_GnO64hD9TnLqBjpzmzKZ2g_J8-9zLZwbuG4psY',
      sheetno = 3,
      dataurl = 'https://spreadsheets.google.com/feeds/list/' + sheetid + '/' + sheetno + '/public/values?alt=json-in-script&callback=?';

  $.getJSON(dataurl, 
  function(json) {
      var d = json.feed.entry;
      n = d.length;

      $(d).each(function() {
        $('.gift-list-item').append('<tr>' + 
          '<th class="item">' + this.gsx$item.$t + '</th>' +
          '<td class="npc01">' + '<img src="' + this.gsx$npc01.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc02">' + '<img src="' + this.gsx$npc02.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc03">' + '<img src="' + this.gsx$npc03.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc04">' + '<img src="' + this.gsx$npc04.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc05">' + '<img src="' + this.gsx$npc05.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc06">' + '<img src="' + this.gsx$npc06.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc07">' + '<img src="' + this.gsx$npc07.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc08">' + '<img src="' + this.gsx$npc08.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc09">' + '<img src="' + this.gsx$npc09.$t + '"class="gift-item-img">' + '</td>' +
          '<td class="npc10">' + '<img src="' + this.gsx$npc10.$t + '"class="gift-item-img">' + '</td></tr>');
          })
    })
})();